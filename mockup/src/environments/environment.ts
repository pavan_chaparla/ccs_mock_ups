// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyARRUNHi3Z_xP-eqcaR95LdtFZwAvAae_M",
    authDomain: "custom-vigil-245712.firebaseapp.com",
    databaseURL: "https://custom-vigil-245712.firebaseio.com",
    projectId: "custom-vigil-245712",
    storageBucket: "custom-vigil-245712.appspot.com",
    messagingSenderId: "173034883325",
    appId: "1:173034883325:web:ffb62ad7287bf33f9b9694"
  }
};


/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.

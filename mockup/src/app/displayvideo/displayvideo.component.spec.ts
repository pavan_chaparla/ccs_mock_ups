import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DisplayvideoComponent } from './displayvideo.component';
import {MatSliderModule, MatSlideToggleModule, MatInputModule, MatFormFieldModule} from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HttpClientModule } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable, of, from  } from 'rxjs';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { environment } from '../../environments/environment';




describe('DisplayvideoComponent', () => {
  let component: DisplayvideoComponent;
  let fixture: ComponentFixture<DisplayvideoComponent>;
  // const fakeActivatedRoute = {
  //   snapshot: {  }
  // } as ActivatedRoute;
  // let de: DebugElement = fixture.debugElement;
  // let de:DebugElement = new DebugElement();
  // const heading = fixture.debugElement.nativeElement.querySelector('.titlefordata');
  // console.log(heading);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DisplayvideoComponent],
  imports: [   MatSliderModule, MatSlideToggleModule, MatInputModule, MatFormFieldModule, AngularFireAuthModule,
     FlexLayoutModule,HttpClientModule,
    AngularFireModule.initializeApp(environment.firebase),
  ],
  providers: [  AngularFireModule, AngularFirestore, AngularFirestoreModule,
    {
      provide: ActivatedRoute, useValue: {
        params: of({ id: 'Yvd0w7912zeNTnfqzJ9Y' })
      }
    }
  ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DisplayvideoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have a title', () => {
    // we are accessing "h1"
    const title = fixture.debugElement.query(By.css('h1')).nativeElement;
    expect(title.innerHTML).toBe('NOTICE BOARD');
  });

  it('should have a title', () => {
    // we are accessing "h1"
    const title = fixture.debugElement.query(By.css('h1')).nativeElement;
    expect(title.innerHTML).toBe('NOTICE BOARD');
  });
});

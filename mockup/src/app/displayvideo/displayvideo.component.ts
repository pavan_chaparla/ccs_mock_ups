import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
type terminals = Array<string>;
type terminalCheck = Array<Array<string>>;
type videoClick = Array<boolean>;
export interface type{
  id: number;
  text:string;
  orderNumber:number;
  orderTime:number;
  transTime: number;
  transStartTime:number;
  transEndTime:number;
  transReported:boolean;
  expandvideo:string;
}

@Component({
  selector: 'app-displayvideo',
  templateUrl: './displayvideo.component.html',
  styleUrls: ['./displayvideo.component.css']
})

export class DisplayvideoComponent implements OnInit {

  terminalDataAppear = false;
  public orderDetails: type[] = [];
  initialCol : string;
  resizedCol : string;
  itemresize : string;
  startTime: Date = new Date(1577103814);
  sum = 0;
  showTextArea = false;
  ordertext : string;
  currentTransactionTime: string;
  ordertranstime : number;
  transaction_display;
  product_display = [];
  thumbLabel = false;
  currentOrderNumber : number;
  // sliderOffsetX :number;
  // sliderOffsetY :number;
  // sliderWidth :number;
  // sliderValAtPos :number;
  // sliderValue: number;
  // sliderval: number;

  orderTime = {};
  orderNumber = {};
  orderTimes = [];
  orderNumbers = [];
  productDetails = [];
  terminalActive: videoClick = [];
  terminalData: terminalCheck = [];
  terminal: terminals = [];
  terminalIds: terminals = [];
  maximized = false;
  size: Number;
  terminalList = [];
  flag: boolean = false;
  videoClicks: videoClick = [];
  paused: boolean = true;
  difTerminal = false;
  videoUrl = "https://drive.google.com/uc?export=view&id=";
  @ViewChild('slider', {static: false}) slider: ElementRef;

  constructor(private http_client: HttpClient, private route: ActivatedRoute,public firestore: AngularFirestore ,private auth: AngularFireAuth) {
    var firestore_id;
    this.auth.authState.subscribe(
      (auth) =>{
        if(auth!=null){
            this.route.params.subscribe( params => {
              firestore_id = params;
            }
            );
            var requestId = this.firestore.collection("user_requests").doc(firestore_id.id);
            requestId.get().subscribe(doc=> {
              this.startTime = doc.data()['transaction_time'];
            })

            var transactionIds = requestId.collection("transaction_logs");
            var docCount = 0;
            //data  display  here...............................

            transactionIds.get().subscribe(snapshot => {snapshot.forEach(doc => {
              docCount+=1;
              var docId = doc.id;
              var posDoc = this.firestore.collection("pos_data").doc(docId);
              this.displayData(posDoc,snapshot.size);
              var data = doc.data();
              this.orderTimes.push(Number(data['order_time']));
              // console.log('ccccccccc',data["terminal_id"],typeof(data["terminal_id"]));
              if(this.orderTime[data["terminal_id"]])
                this.orderTime[data["terminal_id"]].push(Number(data['order_time']));
              else
                this.orderTime[data["terminal_id"]] = [Number(data['order_time'])];  
              // console.log(this.orderTime,"asasasasasas");
              if(this.orderNumber[data["terminal_id"]]){
                this.orderNumber[data["terminal_id"]].push(Number(data['order_number']));
                console.log(this.orderNumber,"oriiii")
              }
              else
                this.orderNumber[data["terminal_id"]] = [Number(data['order_number'])];
              var terminalIdTemp = data["terminal_id"];
              var id = this.terminalIds.indexOf(terminalIdTemp)
              this.terminalActive.push(false);
              console.log(this.terminalData +"terminal data");

              if(id==-1){
                this.terminal = data["drive_urls "];
                this.terminalIds.push(terminalIdTemp);
                if(this.terminal.length>0){
                  for(var i=0;i<this.terminal.length;i++)
                    this.terminal[i] = this.videoUrl+this.getid(this.terminal[i]);
                  var terminalObject = {
                    "driveUrls":this.terminal,
                    "terminalId":terminalIdTemp
                  }
                  this.terminalList.push(terminalObject);
                }
                if(this.terminalList.length>0){
                  var video = this.terminal[0];
                  for(var i=0;i<this.terminal.length;i++){
                    this.videoClicks[i]=false;
                  }
                  this.terminal = this.terminalList[0];
                  // console.log(this.terminal['driveUrls'].length,"kdojudiudiudiudiui");
                  this.gridsize(this.terminal['driveUrls'].length);
                  this.terminalActive[0] = true;

                }
              }

              //transaction bar here.......................
              if(snapshot.size == docCount){
                console.log(this.orderTime,"ighaloooo",data)
                this.projectTransactionBar([this.orderTime[this.terminal["terminalId"]],this.orderNumber[this.terminal["terminalId"]]]);
              }
            })
          });

        }
        else{
          var provider = new firebase.auth.GoogleAuthProvider();
          firebase.auth().signInWithPopup(provider).then(function(result) {
            // console.log(result);
          })
        }
      });
  }

  ngOnInit() {
    
  }

  async displayData(posDoc,snapshot_size){
    console.log("[[[[")
    var posData = await posDoc.get().subscribe(doc=>{
      var temp = [];
      temp[0] = (doc.data()['POSOutput']['terminal']);
      temp[1] = doc.data()['POSOutput']['Order']['MainOrder'];
      temp[2] = doc.data()['order_time']['seconds'].toString();
      temp[3] = doc.data()['POSOutput']['Order']['Subtotal'];
      temp[4] = doc.data()['POSOutput']['Order']['Taxes'];
      temp[5] = doc.data()['POSOutput']['Order']['Total'];
      temp[6] = doc.data()['POSOutput']['employee_badge'];
      temp[7] = doc.data()['POSOutput']['Order']['Payments']['Payment']['name'];
      temp[8] = doc.data()['POSOutput']['Order']['number'];
      this.terminalData.push(temp);
      if(this.terminalData.length == snapshot_size){
        // console.log("uuuuuuuuuuu")
        for(var i = 0;i<snapshot_size;i++){
          var temp_product_details = {};
          var product_array = [];
          var valuemeal_array = [];
          var valuemeal_products = [];
          if(this.terminalData[i][1]['ProductDetail']['Product']){
            //Check if the product length is greater than 1
            if(this.terminalData[i][1]['ProductDetail']['Product'].length){
              for(var j = 0;j<this.terminalData[i][1]['ProductDetail']['Product'].length;j++){
                 product_array.push({
                  "name":this.terminalData[i][1]['ProductDetail']['Product'][j]['name'],
                  "price":this.terminalData[i][1]['ProductDetail']['Product'][j]['price'],
                  "extended_price":this.terminalData[i][1]['ProductDetail']['Product'][j]['extended_price']
                })
              }
            }
             //Check if the product doesnot have array
            if(this.terminalData[i][1]['ProductDetail']['Product']['name']){
              product_array.push({
                "name":this.terminalData[i][1]['ProductDetail']['Product']['name'],
                "price":this.terminalData[i][1]['ProductDetail']['Product']['price'],
                "extended_price":this.terminalData[i][1]['ProductDetail']['Product']['extended_price']
              })
            }
          }
          if(this.terminalData[i][1]['ProductDetail']['ValueMeal']){
          //Check for valueMeal array products
            if(this.terminalData[i][1]['ProductDetail']['ValueMeal'].length){
              for(var j=0;j<this.terminalData[i][1]['ProductDetail']['ValueMeal'].length;j++){
                for(var k = 0; k<this.terminalData[i][1]['ProductDetail']['ValueMeal'][j]['Product'].length;k++){
                  //valuemeal_products.push(this.terminalData[i][1]['ProductDetail']['ValueMeal'][j]['Product'][k]['name']);
                  var temp1 = {
                    "name" : this.terminalData[i][1]['ProductDetail']['ValueMeal'][j]['Product'][k]['name'],
                    "price": this.terminalData[i][1]['ProductDetail']['ValueMeal'][j]['Product'][k]['price'],
                    "extended_price": this.terminalData[i][1]['ProductDetail']['ValueMeal'][j]['Product'][k]['extended_price']
                  }
                  valuemeal_products.push(temp1);
                }
                valuemeal_array.push({
                  "name": this.terminalData[i][1]['ProductDetail']['ValueMeal'][j]["name"],
                  "price": this.terminalData[i][1]['ProductDetail']['ValueMeal'][j]["price"],
                  "extended_price": this.terminalData[i][1]['ProductDetail']['ValueMeal'][j]["extended_price"]
                })
              }
            }
            //Check for valuemeal products directly
            if(this.terminalData[i][1]['ProductDetail']['ValueMeal']['Product']){
              for(var k = 0; k<this.terminalData[i][1]['ProductDetail']['ValueMeal']['Product'].length;k++){
                //valuemeal_products.push(this.terminalData[i][1]['ProductDetail']['ValueMeal'][j]['Product'][k]['name']);
                var temp2 = {
                  "name" : this.terminalData[i][1]['ProductDetail']['ValueMeal']['Product'][k]['name'],
                  "price": this.terminalData[i][1]['ProductDetail']['ValueMeal']['Product'][k]['price'],
                  "extended_price": this.terminalData[i][1]['ProductDetail']['ValueMeal']['Product'][k]['extended_price']
                }
                valuemeal_products.push(temp2);
              }
              valuemeal_array.push({
                "name": this.terminalData[i][1]['ProductDetail']['ValueMeal']["name"],
                "price": this.terminalData[i][1]['ProductDetail']['ValueMeal']["price"],
                "extended_price": this.terminalData[i][1]['ProductDetail']['ValueMeal']["extended_price"]
              })
            }
          }

            temp_product_details = {
              "Ordertime":this.terminalData[i][2],
              "Taxes":this.terminalData[i][4],
              "SubTotal":this.terminalData[i][3],
              "Terminal":this.terminalData[i][0],
              "Products":product_array,
              "Valuemeal_products":valuemeal_products,
              "Valuemeals":valuemeal_array,
              "Total":this.terminalData[i][5],
              "Employeebadge":this.terminalData[i][6],
              "PaymentMode": this.terminalData[i][7],
              "OrderNumber":this.terminalData[i][8]
            };
            this.product_display.push(temp_product_details);
            console.log(this.product_display,"oooooooooo")
          }
        }
      if(this.product_display.length == snapshot_size){
        this.terminalDataAppear = true;
        this.product_display = this.product_display.sort((a,b) => {
          return Number(a.Ordertime) - Number(b.Ordertime);
        });
         console.log("popopop",this.product_display);
      }
    });
  }

  async projectTransactionBar(ordertimesnumbers){
    this.orderDetails = [];
    console.log(ordertimesnumbers,"pola")
    this.orderTimes = ordertimesnumbers[0];
    this.orderTimes.sort();
    this.orderNumbers = ordertimesnumbers[1];
    this.orderNumbers.sort();
    this.sum = 0;
    var temp : type = {
        id : 0,text: ' ',orderTime: 0, orderNumber: 0,transTime:0,transStartTime:0,transEndTime:0, transReported: false,expandvideo: ""
    }
    temp.id = 0;
    temp.orderTime = this.orderTimes[0];
    temp.orderNumber = this.orderNumbers[0];
    temp.transTime = (new Date(this.orderTimes[0]).getTime() - (new Date(this.startTime)).getTime()) / 6;
    temp.transEndTime = temp.transTime;
    temp.transStartTime = this.sum;
    this.orderDetails.push(temp);
    this.sum = this.orderDetails[0].transTime;
    for(var i = 1; i < this.orderTimes.length; i++) {
        temp = {id : 0,text: ' ',orderTime: 0,orderNumber:0,transTime:0,transStartTime:0,transEndTime:0, transReported: false,expandvideo: ""};
        temp.id = i;
        temp.orderTime = this.orderTimes[i];
        temp.orderNumber = this.orderNumbers[i];
        temp.transTime = (new Date(this.orderTimes[i]).getTime() - new Date(this.orderTimes[i - 1]).getTime()) / 6;
        temp.transStartTime = this.sum;
        this.orderDetails.push(temp);
        if(i<this.orderTimes.length-1){
            this.sum += this.orderDetails[i].transTime;
        }
        temp.transEndTime = this.sum;
        console.log(this.orderDetails,"kjdjdidijdi");
    }
    this.orderDetails[this.orderDetails.length-1].transEndTime = 100;
    if(this.orderTimes.length>1){
      this.orderDetails[this.orderDetails.length-1].transTime = 100 - this.sum;
    }
    else{
      this.orderDetails[this.orderDetails.length-1].transTime = 100;
    }
  }

  onPlaying(event: any){
    var tmp = document.getElementsByClassName("my_video");
    var videos = Array.prototype.slice.call( tmp, 0 );
    if(this.difTerminal){
      var _this = this;
      videos.map(function(video){
        // console.log((_this.slider['value']/100)*video.duration,_this.slider['value']);
        video.currentTime = (_this.slider['value']/100)*video.duration;
      })
      this.difTerminal = false;
    }
    this.slider['value'] = (videos[0].currentTime/videos[0].duration)*100;
    // this.sliderval = this.slider['value'];

    // this.slider.nativeElement.value = (videos[0].currentTime/videos[0].duration)*100;
    
    for(let i = 0; i < this.orderDetails.length;i++ ){
      var k = 0;
      for(let j=0; j < this.product_display.length; j++){
        // console.log(this.orderDetails[i]["orderTime"].toString(),"&&&&&&&&&&&&&&&&&&&&&",this.product_display[j]["Ordertime"])
        if(this.slider['value'] >= this.orderDetails[i].transStartTime && this.slider['value'] <= this.orderDetails[i].transEndTime && this.orderDetails[i]["orderTime"].toString() == this.product_display[j]["Ordertime"]) {
          this.currentTransactionTime = (new Date(this.product_display[j]["Ordertime"]*1000)).toUTCString();         
          this.transaction_display = this.product_display[j];
          this.currentOrderNumber = this.orderDetails[i]['orderNumber'];
          k = 1;
          break;
         }
      }
      if(k == 1){
        break;
      }
    }

  }

  // getTimeHover(event : any){
    
  //   var slidertitle = document.getElementById("slidertitle");
  //   this.sliderOffsetX = this.slider.nativeElement.getBoundingClientRect().left - document.documentElement.getBoundingClientRect().left;
  //   this.sliderOffsetY = this.slider.nativeElement.getBoundingClientRect().top - document.documentElement.getBoundingClientRect().top;
  //   this.sliderWidth = this.slider.nativeElement.offsetWidth - 1;
  //   var currentMouseXPos = (event.clientX + window.pageXOffset) - this.sliderOffsetX;
  //   this.sliderValAtPos = Math.round(currentMouseXPos / this.sliderWidth * 100 + 1);
  //   this.sliderValue = Math.round(currentMouseXPos / this.sliderWidth * 100 + 1)*6;
  //   if(this.sliderValAtPos < 0) this.sliderValAtPos = 0;
  // // ... and this are to make it easier to hover on the "0" and "100" positions
  // slidertitle.style.top = this.sliderOffsetY - 120 + 'px';
  // slidertitle.style.left = currentMouseXPos + 'px';

    
  // }

  setSlider(transStartTime){
    // alert(transStartTime);
    this.slider['value'] = transStartTime;
    this.onInputChange(event);
  }
  showText(){
    this.showTextArea = !this.showTextArea ;
  }
  onInputChange(event: any){
    var tmp = document.getElementsByClassName("my_video");
    var videos = Array.prototype.slice.call( tmp, 0 );
    var _this = this;
    videos.map(function(video){
      console.log((_this.slider['value']/100)*video.duration,_this.slider['value']);
      video.currentTime = (_this.slider['value']/100)*video.duration;
    })
  }
  playVideos(event:any){
    this.paused = false;
    var tmp = document.getElementsByClassName("my_video");
    var videos = Array.prototype.slice.call( tmp, 0 );
    videos.map(function(video){
      video.play();
    })
  }
  pauseVideos(event:any){
    this.paused = true;
    var tmp = document.getElementsByClassName("my_video");
    var videos = Array.prototype.slice.call( tmp, 0 );
    videos.map(function(video){
      video.pause();
    })
  }
  getid(url){
    return url.match(/[-\w]{25,}/);
  }
  gridClick(event:any){
    this.flag=true;
    this.maximized = true;
    for(var i=0;i<this.videoClicks.length;i++){
      if(i==event.target.id){
        this.videoClicks[i]=true;
      }
      else
      this.videoClicks[i]=false;
    }
  }
  async terminalClick(event:any){
    this.paused = true;
    this.difTerminal = await true;
    var tmp = document.getElementsByClassName("my_video");
    var videos = Array.prototype.slice.call( tmp, 0 );
    var time = videos[0].currentTime;
    this.slider['value'] = (videos[0].currentTime/videos[0].duration)*100;
    var clickedVideo = event.target.id;
    this.gridsize(this.terminalList[clickedVideo].driveUrls.length);
    
    for(var i=0;i<this.terminalActive.length;i++)
      this.terminalActive[i] = false;
    this.terminalActive[clickedVideo] = true;
    this.terminal = await this.terminalList[clickedVideo];
    this.slider['value'] = await (time/videos[0].duration)*100;
    this.projectTransactionBar([this.orderTime[this.terminal["terminalId"]],this.orderNumber[this.terminal["terminalId"]]]);
  }
  onSubmit(transactionTime) {
    console.log(transactionTime);
    for(let i = 0; i < this.orderDetails.length;i++) {
      // tslint:disable-next-line:max-line-length
     if(this.orderDetails[i].transTime == transactionTime){
       alert('report');
      this.orderDetails[i].transReported = true;
        break;
     }
     console.log(this.orderDetails);
    }
  }
  minimize(event:any){
    this.flag=false;
    this.maximized = false;
    for(var i=0;i<this.videoClicks.length;i++){
      this.videoClicks[i]=false;
    }
  }

  gridsize(gridlength:number){
    if(gridlength > 6){
      this.initialCol = "gridcol7";
    }else if(gridlength > 4 ){
      this.initialCol = "gridcol3";
    }else if(gridlength == 4 ){
      this.initialCol = "gridcol2";
    }else if(gridlength == 1 ){
      this.initialCol = "gridcol1";
    }else if(gridlength == 2){
      this.initialCol = "gridcol12";
    }else{
      this.initialCol = "gridcol2";
    }
    if(gridlength > 7){
      this.resizedCol = "gridresizecol4";
      this.itemresize = "itemresizecol4";
    }else if(gridlength > 5){
      this.resizedCol = "gridresizecol3";
      this.itemresize = "itemresizecol3";
    }else if(gridlength == 5){
      this.resizedCol = "gridresizecol4";
      this.itemresize = "itemresizecol4";
    }else{
      this.resizedCol = "gridresizecol3";
      this.itemresize = "itemresizecol3";
    }
  }
}
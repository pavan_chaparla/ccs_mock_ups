import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DisplayvideoComponent } from './displayvideo/displayvideo.component';

const routes: Routes = [
  {path: 'home/:id', component: DisplayvideoComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
